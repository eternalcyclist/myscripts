#!/usr/bin/python

import os,sys
from subprocess import Popen,PIPE

def read_clipboard():
    cl = Popen("wl-paste",stdout=Pipe)
    return cl.stdout.readline().strip()

if len(sys.argv)>1:
    if sys.argv[1]=="-c":
        word = read_clipboard()
    else:
        word = sys.argv[1]
else:
    din = Popen("dmenu -p Translate",shell=True,stdin=PIPE, stdout=PIPE)
    din.stdin.close()
    word = din.stdout.readline()
print(word)
dict = Popen("dict %s"%word,shell=True,stdout=PIPE)
ans = dict.stdout.readlines()
d=""
lc=0
transd={}
dout = Popen("dmenu",shell=True,stdin=PIPE,stdout=PIPE)
for line in ans:
    if line.startswith("From"):
        d=line+"\n"
        lc=0
        continue
    lc+=1
    if d!="":
        if lc==3:
            transl = line.strip()
            transd[transl]=d
            print(transl)
            dout.stdin.write(transl+"\n")
            d=""
            continue
    d+=line+"\n"
        
dout.stdin.close()
ans = dout.stdout.readline()
ans = ans.strip()
print(ans)
os.system("wl-copy '%s'"%ans)
if len(ans)!=0:
    print(transd[ans])

