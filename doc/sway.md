# Sway

## Key Bindings

### Leader Key
The windows key is the leader key. Maybe I should switch to Spacebar. But I am using Spacebar as Leader Key in vim.

## System Menu
The system menu is activated with the power buttton. In order to work correctly the power button must
be deactivated in /etc/systemd/login.config:
   *P HandlePowerKey#ignore *P
  


## py3status instead of swaybar

## Running Python Scripts

For unkown reasons, sway does not execute my python scripts directly despite the correctly set shebang.
Therefore I need to use
  [...] exec /usr/bin/python myscript.py


