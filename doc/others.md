# Other Scripts

## dict.py

dict.py is a wrapper for dict which uses dmenu as input and output interface. 
In sway config it is bound to Ctrl-t. 
After pressing this key combo it asks for the word to translate.
It presents a list of translations. 

Select the one you want either with the arrow keys or by typing the first letters and press Enter.
This should put the translation into clipboard.



