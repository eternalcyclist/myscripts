syntax on
set tabstop=4 softtabstop=4
set shiftwidth=4
set noerrorbells
set smartindent
set smartcase
set incsearch
set encoding=utf-8

call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree'
Plug 'flazz/vim-colorschemes'
Plug 'https://github.com/felixhummel/setcolors.vim'
"Plug 'morhetz/gruvbox'
Plug 'ycm-core/youcompleteme'
Plug 'mbbill/undotree'
Plug 'vim-utils/vim-man'
Plug 'ervandew/supertab'
Plug 'tpope/vim-fugitive'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'jmanoel7/vim-games'
"Plug 'tmhedberg/simpylfold'
Plug 'vim-scripts/indentpython.vim'
call plug#end()

colorscheme tt
"colorscheme murphy
"colorscheme ron
"colorscheme borland
"colorscheme zellner
"colorscheme pablo
"colorscheme delek
"colorscheme industry
"colorscheme koehler
"colorscheme elflord
"colorscheme elflord
"colorscheme slate
"colorscheme darkblue
"colorscheme dw_green
"colorscheme night 

let mapleader = " "
nnoremap <leader>h : wincmd h<CR>
nnoremap <leader>j : wincmd j<CR>
nnoremap <leader>k : wincmd k<CR>
nnoremap <leader>l : wincmd l<CR>
nnoremap <leader>u : UndotreeShow<CR>
nnoremap <leader><CR> : wincmd ]<CR>
nnoremap <leader>n : bnext<CR>
nnoremap <leader>p : bprevious<CR>
nnoremap <leader>o : only<CR>
nnoremap <leader>c : bd<CR>
nnoremap <buffer> <silent> <leader>rr :YcmCompleter RefactorRename<space>
nnoremap <buffer> <silent> <leader>gd :YcmCompleter GoTo<CR>
nnoremap <buffer> <silent> <leader>gr :YcmCompleter GoToReferences<CR>

filetype plugin on
filetype indent on
set grepprg=grep\ -nH\ $*
set expandtab
set magic
set viminfo='1000,f1,/25,!
set nowrap
set laststatus=2
set statusline=%c.%l\ %t
set modeline
"if has("autocmd")
"	 autocmd  FileType python set complete+=k~/.vim/pydiction iskeyword+=.,(
"endif " has("autocmd")

autocmd BufRead *.py set makeprg=python\ -c\ \"import\ py_compile,sys;\ sys.stderr=sys.stdout;\ py_compile.compile(r'%')\"
autocmd BufRead *.py set efm=%C\ %.%#,%A\ \ File\ \"%f\"\\,\ line\ %l%.%#,%Z%[%^\ ]%\\@=%m
autocmd BufRead *.py nmap <F5> :!python %<CR>

let g:syntastic_mode_map = {
    \ "mode": "passive",
    \ "active_filetypes": ["ruby", "php"],
    \ "passive_filetypes": ["python"] }

au! BufRead,BufNewFile *.moin set filetype=moin 

"au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
"BadWhitespace gibbs nicht

:map � :
:map � :
:map <space> :

" warum geht das folgende nicht!?
:map <guillemotleft> <bar>

" lynx Kompatibilit�t
:map <left> <C-T>
:map <right> <C-]>

:imap <up> <esc>i

:map <F9> make<CR>
